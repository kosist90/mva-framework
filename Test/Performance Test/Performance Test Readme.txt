Performance Test Readme *******************************************************************************

Performance Testing (i.e. Benchmarking) is used to quantify the performance of the running framework
under various operating conditions and use cases. These tests probably launch way too many Actors, just
to see what happens. You know, that kind of thing. The tests are (usually?) automated. Failures might
be dismissed as acceptable after passing the devloper's scrutiny.

Contact ethan.stern@composed.io